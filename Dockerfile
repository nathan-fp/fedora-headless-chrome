FROM fedora
COPY chrome.repo ./chrome.repo
RUN dnf install -y xorg-x11-server-Xvfb
RUN cat chrome.repo > /etc/yum.repos.d/google-chrome.repo
RUN dnf install -y google-chrome-stable
RUN dnf install -y chromedriver
RUN python3 -m venv .pyenv
RUN source .pyenv/bin/activate
CMD ['python']
